package eu.switchproject.probes.cgroups;

import eu.celarcloud.jcatascopia.probepack.Probe;
import eu.celarcloud.jcatascopia.probepack.ProbeMetric;
import eu.celarcloud.jcatascopia.probepack.ProbePropertyType;
import eu.switchproject.probes.cgroups.CGroups.CGroup;
import org.reflections.Reflections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;


/**
 * CGroupProbe for JCatascopia
 * Gathers metrics, statistics and settings from the current cgroup
 */
public class CGroupProbe extends Probe {

    private static int DEFAULT_SAMPLING_PERIOD = 10;
    private static String DEFAULT_PROBE_NAME = "CGroupProbe";

    private Map<String, CGroup> cgroups = new HashMap<>();
    private int newPropertyNumber = 0;
    private Map<String, Integer> statmap = new HashMap<>();


    /**
     * Finds all CGroup subclasses and iterates through them
     * It then adds a property to the probe for each metric in each class
     * @param name Probe Name
     * @param freq Sampling Frequency
     */
    public CGroupProbe(String name, int freq) {
        super(name, freq);

        Reflections reflections = new Reflections("eu.switchproject.probes.cgroups.CGroups");

        Set<Class<? extends CGroup>> subTypes = reflections.getSubTypesOf(CGroup.class);

        for (Class<? extends CGroup> cls : subTypes) {
            CGroup cg = null;
            try {
                cg = cls.newInstance();
                String cgname = cg.getName();

                for (String s : cg.getStatNames()) {
                    addProbeProperty(cgname + "." + s, ProbePropertyType.LONG, "", s);
                }

                cgroups.put(cgname, cg);
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public CGroupProbe() {
        this(DEFAULT_PROBE_NAME, DEFAULT_SAMPLING_PERIOD);
    }

    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        CGroupProbe p = new CGroupProbe();
        p.activate();
    }

    /**
     * add property while tracking property id and name mappings
     * @param propName
     * @param propType
     * @param propUnits
     * @param desc
     */
    private void addProbeProperty(String propName, ProbePropertyType propType, String propUnits, String desc) {
        int propnum = newPropertyNumber++;
        addProbeProperty(propnum, propName, propType, propUnits, desc);
        statmap.put(propName, propnum);
    }

    @Override
    public ProbeMetric collect() {
        HashMap<Integer, Object> values = new HashMap<Integer, Object>();

        for (Map.Entry<String, CGroup> entry : cgroups.entrySet()) {
            CGroup cg = entry.getValue();
            String name = entry.getKey();

            for (String s : cg.getStatNames()) {
                Long value = 0l;
                try {
                    value = Long.parseLong(cg.getStat(s));
                } catch (Exception ignored) {

                }
                values.put(statmap.get(name + "." + s), value);
            }
        }

        return new ProbeMetric(values);
    }

    @Override
    public String getDescription() {
        return "CGroupProbe for JCatascopia";
    }

    @Override
    public String metricToJSON(ProbeMetric metric){
        String json = super.metricToJSON(metric);
        writeToProbeLog(Level.INFO, json);
        return json;
    }
}