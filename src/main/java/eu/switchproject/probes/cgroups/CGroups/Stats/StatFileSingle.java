package eu.switchproject.probes.cgroups.CGroups.Stats;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class StatFileSingle extends Stat {
    private Path path;

    public StatFileSingle(String path, String file) {
        this.path = Paths.get(path, file);
    }

    public String getStat() {
        if(Files.exists(path)) {
            try (BufferedReader reader = Files.newBufferedReader(path)) {
                String line = null;
                StringBuilder sb = new StringBuilder();

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                return sb.toString();
            } catch (Exception x) {
                System.err.format("Exception: %s%n", x);
            }
        }

        return "0";
    }
}
