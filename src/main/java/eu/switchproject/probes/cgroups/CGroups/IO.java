package eu.switchproject.probes.cgroups.CGroups;

public class IO extends CGroup {
    public IO() {
        super();
        name = "IO";
        path = BASE_PATH + "blkio/";

        addStatFileSingle("memory.usage_in_bytes");
        addStatFileMulti("total", "blkio.io_queued_recursive");
        addStatFileMulti("total", "blkio.io_service_time_recursive");
        addStatFileMulti("total", "blkio.io_wait_time_recursive");
        addStatFileMulti("total", "blkio.throttle.io_service_bytes");
        addStatFileMulti("total", "blkio.throttle.io_serviced");
    }
}
