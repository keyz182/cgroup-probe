package eu.switchproject.probes.cgroups.CGroups.Stats;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class StatFileMulti extends Stat {
    private Path path;
    private String statname;

    public StatFileMulti(String path, String file, String statname) {
        this.path = Paths.get(path, file);
        this.statname = statname;
    }

    public String getStat() {
        if(Files.exists(path)) {
            try (BufferedReader reader = Files.newBufferedReader(path)) {
                String line = null;
                while ((line = reader.readLine()) != null) {
                    if (line.toLowerCase().startsWith(statname)) {
                        reader.close();
                        return line.replace(statname, "").trim();
                    }
                }
            } catch (Exception x) {
                System.err.format("Exception: %s%n", x);
            }
        }
        return "0";
    }
}
