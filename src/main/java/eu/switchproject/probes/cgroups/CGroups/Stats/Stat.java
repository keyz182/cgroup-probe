package eu.switchproject.probes.cgroups.CGroups.Stats;

public abstract class Stat {
    public abstract String getStat();
}
