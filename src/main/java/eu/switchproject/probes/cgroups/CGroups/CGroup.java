package eu.switchproject.probes.cgroups.CGroups;

import eu.switchproject.probes.cgroups.CGroups.Stats.Stat;
import eu.switchproject.probes.cgroups.CGroups.Stats.StatFileMulti;
import eu.switchproject.probes.cgroups.CGroups.Stats.StatFileSingle;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Abstract base class for various cgroup endpoints
 */
public abstract class CGroup {
    static String BASE_PATH = "/sys/fs/cgroup/";
    String path;
    String name;
    private Map<String, Stat> stats;

    CGroup() {
        stats = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    /**
     * Add a stat where the file contains only one stat
     * @param filename name of the file containing the stat
     */
    void addStatFileSingle(String filename) {
        this.stats.put(filename, new StatFileSingle(path, filename));
    }

    /**
     * Add a stat where the file contains multiple stats in the form "[name] [val]"
     * @param statname the name of the stat
     * @param filename name of the file containing the stat
     */
    void addStatFileMulti(String statname, String filename) {
        this.stats.put(statname, new StatFileMulti(path, filename, statname));
    }

    public String getStat(String name) {
        return stats.get(name).getStat();
    }

    public Set<String> getStatNames() {
        return stats.keySet();
    }
}
