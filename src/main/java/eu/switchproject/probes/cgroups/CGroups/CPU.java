package eu.switchproject.probes.cgroups.CGroups;

public class CPU extends CGroup {
    public CPU() {
        super();
        name = "CPU";
        path = BASE_PATH + "cpu/";

        addStatFileSingle("cpu.shares");
        addStatFileMulti("nr_periods", "cpu.stat");
        addStatFileMulti("nr_throttled", "cpu.stat");
        addStatFileMulti("throttled_time", "cpu.stat");
    }
}
