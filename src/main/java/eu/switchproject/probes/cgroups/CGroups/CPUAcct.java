package eu.switchproject.probes.cgroups.CGroups;

public class CPUAcct extends CGroup {
    public CPUAcct() {
        super();
        name = "CPUAcct";
        path = BASE_PATH + "cpu/";

        addStatFileSingle("cpuacct.usage");
        addStatFileMulti("user", "cpuacct.stat");
        addStatFileMulti("system", "cpuacct.stat");
    }
}
