package eu.switchproject.probes.cgroups.CGroups;

public class Memory extends CGroup {
    public Memory() {
        super();
        name = "Memory";
        path = BASE_PATH + "memory/";

        addStatFileSingle("memory.usage_in_bytes");
        addStatFileSingle("memory.memsw.usage_in_bytes");
        addStatFileSingle("memory.failcnt");
        addStatFileSingle("memory.limit_in_bytes");
        addStatFileMulti("cache", "memory.stat");
        addStatFileMulti("rss", "memory.stat");
        addStatFileMulti("rss_huge", "memory.stat");
        addStatFileMulti("mapped_file", "memory.stat");
        addStatFileMulti("dirty", "memory.stat");
        addStatFileMulti("writeback", "memory.stat");
        addStatFileMulti("swap", "memory.stat");
        addStatFileMulti("pgpgin", "memory.stat");
        addStatFileMulti("pgpgout", "memory.stat");
        addStatFileMulti("pgfault", "memory.stat");
        addStatFileMulti("pgmajfault", "memory.stat");
        addStatFileMulti("inactive_anon", "memory.stat");
        addStatFileMulti("active_anon", "memory.stat");
        addStatFileMulti("inactive_file", "memory.stat");
        addStatFileMulti("active_file", "memory.stat");
        addStatFileMulti("unevictable", "memory.stat");
        addStatFileMulti("hierarchical_memory_limit", "memory.stat");
        addStatFileMulti("hierarchical_memsw_limit", "memory.stat");
        addStatFileMulti("total_cache", "memory.stat");
        addStatFileMulti("total_rss", "memory.stat");
        addStatFileMulti("total_rss_huge", "memory.stat");
        addStatFileMulti("total_mapped_file", "memory.stat");
        addStatFileMulti("total_dirty", "memory.stat");
        addStatFileMulti("total_writeback", "memory.stat");
        addStatFileMulti("total_swap", "memory.stat");
        addStatFileMulti("total_pgpgin", "memory.stat");
        addStatFileMulti("total_pgpgout", "memory.stat");
        addStatFileMulti("total_pgfault", "memory.stat");
        addStatFileMulti("total_pgmajfault", "memory.stat");
        addStatFileMulti("total_inactive_anon", "memory.stat");
        addStatFileMulti("total_active_anon", "memory.stat");
        addStatFileMulti("total_inactive_file", "memory.stat");
        addStatFileMulti("total_active_file", "memory.stat");
        addStatFileMulti("total_unevictable", "memory.stat");

    }
}
